Software created on the image of Tristar, a crossroad management application.

This is a RESTful web page that contains both machine to machine communication and a fully integrated administration panel for the user. 
In creation along with the standard technologies I've used elements for symfony such as Dbal and Twig.
Additionally I've added some unit test.