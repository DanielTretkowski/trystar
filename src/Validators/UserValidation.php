<?php

namespace Sda\Trystar\Validators;

use Doctrine\DBAL\Connection;
use Sda\Trystar\Client\Client;
use Sda\Trystar\Response\Response;

class UserValidation
{
    /**
     * @var Connection
     */
    private $dbh;
    /**
     * @var Client
     */
    private $client;
    /**
     * @var Response
     */
    private $response;

    /**
     * UserValidation constructor.
     * @param Connection $dbh
     * @param Response $response
     */
    public function __construct(Connection $dbh, Response $response)
    {

        $this->dbh = $dbh;
        $this->response = $response;
    }

    /**
     * @param string $name
     * @param string $password
     * @return mixed
     */
    public function logInUser($name, $password)
    {
        if (true == $this->checkIfUserExists($name)) {
            if ($this->checkIfPasswordIsCorrect($password)) {
                return '1';
            } else {
                return '3';
            }
        }
        return '2';
    }

    /**
     * @param string $userName
     * @param string $userPass
     * @param string $userPassRep
     */
    public function registerNewUser($userName, $userPass, $userPassRep)
    {
        if ($userName != "") {
            if ($userPass === $userPassRep && $userPass != '') {
                if ($this->checkIfUserExists($userName) == null) {

                    try {
                        $this->dbh->insert('users', array(
                            "User_Name" => $userName,
                            "User_Pass" => $userPass,
                            "Api_Key" => $this->generateRandomUserApiKey(32)

                        ));

                    } catch (\Exception $e) {
                $this->response->send('Ups, we have a problem with our DataBase', Response::STATUS_NOT_FOUND);
                    }
                } else {
                    $this->response->send('User name is taken', 1);
                }
            } else {
                $this->response->send('Your Password must be the same', 2);
            }
        } else {
            $this->response->send('User name can not be empty', 1);
        }
    }

    /**
     * @param string $name
     * @return Client
     */
    private function checkIfUserExists($name)
    {

        $sth = $this->dbh->prepare('SELECT * FROM `users` WHERE `User_Name` = :name');
        $sth->bindValue('name', $name, \PDO::PARAM_STR);
        $sth->execute();
        $data = $sth->fetch();

        $this->client = new Client($data['User_ID'], $data['User_Name'], $data['User_Pass'], $data['LoginDate'], $data['Api_Key']);

        if (null !== $this->client->getUserName()) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * @param string $password
     * @return bool
     */
    private function checkIfPasswordIsCorrect($password)
    {
        if ($this->client->getUserPass() === $password) {
            return true;
        };
    }

    /**
     * @param int $length
     * @return string
     */
    private function generateRandomUserApiKey($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @param string $apiKey
     * @return bool
     */
    public function clientKeyValidation($apiKey){

        $sth = $this->dbh->prepare('SELECT * FROM `users` WHERE `Api_Key` = :Api_Key');
        $sth->bindValue('Api_Key', $apiKey, \PDO::PARAM_STR);
        $sth->execute();
        $data = $sth->fetch();

        if($data!= null) {
            return true;
        }
    }


}