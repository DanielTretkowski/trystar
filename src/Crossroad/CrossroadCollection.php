<?php

namespace Sda\Trystar\Crossroad;


use Sda\Trystar\TypedCollection;

class CrossroadCollection extends TypedCollection
{
    /**
     * CrossroadCollection constructor.
     */
    public function __construct()
    {
        $this->setItemType(Crossroad::class);
    }
}