<?php

namespace Sda\Trystar\Crossroad;

class Crossroad implements \JsonSerializable
{
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var string $name
     */
    private $name;
    /**
     * @var string $securityKey
     */
    private $securityKey;
    /**
     * @var string
     */
    private $url;

    /**
     * Crossroad constructor.
     * @param int $id
     * @param string $name
     * @param string $securityKey
     * @param string $url
     */
    public function __construct($id, $name, $securityKey, $url)
    {
        $this->id = $id;
        $this->name = $name;
        $this->securityKey = $securityKey;
        $this->url = $url;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getSecurityKey()
    {
        return $this->securityKey;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
