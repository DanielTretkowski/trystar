<?php


namespace Sda\Trystar\Crossroad;

use Doctrine\DBAL\Connection;


class CrossroadRepository
{
    /**
     * @var Connection
     */
    private $dbh;

    /**
     * CrossroadRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param int $page
     * @return CrossroadCollection
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAllCrossroads($page=1)
    {
        $pageLimit = ($page*5)-5;
        $sth = $this->dbh->prepare('SELECT * FROM `crossroads` LIMIT :page, 5');
        $sth->bindValue('page', $pageLimit, \PDO::PARAM_INT);
        $sth->execute();

        $crossroadsData = $sth->fetchAll();

        $crossroads = new CrossroadCollection();

        foreach ($crossroadsData as $row) {
            $crossroad = new Crossroad(
                $row['id'],
                $row['name'],
                $row['security_key'],
                $row['URL']
            );
            $crossroads->add($crossroad);
        }
        return $crossroads;
    }
    /**
     * @return CrossroadCollection
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAllCrossroadsNoLimit()
    {
        $sth = $this->dbh->prepare('SELECT * FROM `crossroads`');
        $sth->execute();

        $crossroadsData = $sth->fetchAll();

        $crossroads = new CrossroadCollection();

        foreach ($crossroadsData as $row) {
            $crossroad = new Crossroad(
                $row['id'],
                $row['name'],
                $row['security_key'],
                $row['URL']
            );
            $crossroads->add($crossroad);
        }
        return $crossroads;
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function countRows(){

        $sth = $this->dbh->prepare('SELECT COUNT(*) FROM `crossroads`');
        $sth->execute();
        return $sth->fetchColumn();

    }

    /**
     * @param string $crossroadName
     * @param string $green_red
     * @param string $yellow
     */
    public function addNewCrossroad($crossroadName, $green_red, $yellow)
    {

        $greenTime = $this->getIntOfString($green_red);
        $yellowTime = $this->getIntOfString($yellow);

        $this->addIntoCrossroads($crossroadName);
        $crossRoadAddedID = $this->getCrossroadID($crossroadName);
        $this->addIntoLights($crossRoadAddedID);
        $this->addIntoPhases($crossRoadAddedID,$greenTime,$yellowTime);
    }

    /**
     * @param int $crossroadID
     * @param string $crossroadName
     * @param int $green_red
     * @param int $yellow
     * @return bool
     */
    public function editCrossroad($crossroadID, $crossroadName, $green_red, $yellow){
        if($this->getCrossroadByID($crossroadID) !='') {
            $this->dbh->update('crossroads', array('name' => $crossroadName), array('id' => $crossroadID));
            $this->dbh->update('phases', array('delay' => $green_red), array('crossroads_id' => $crossroadID, 'phase_order' =>1));
            $this->dbh->update('phases', array('delay' => $yellow), array('crossroads_id' => $crossroadID, 'phase_order' =>2));
            $this->dbh->update('phases', array('delay' => $green_red), array('crossroads_id' => $crossroadID, 'phase_order' =>3));
            $this->dbh->update('phases', array('delay' => $yellow), array('crossroads_id' => $crossroadID, 'phase_order' =>4));
        }else{
            return false;
        }
    }

    /**
     * @param int $crossroadID
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function deleteCrossroad($crossroadID){
        $this->dbh->delete('crossroads', array('id'=>$crossroadID));
    }

    /**
     * @param int $length
     * @return string
     */
    private function generateRandomString($length = 20)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @param string $stringToChange
     * @return int
     */
    private function getIntOfString($stringToChange)
    {
        return (int)substr($stringToChange, 0, strrpos($stringToChange, ' '));
    }

    /**
     * @param string $crossroadName
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getCrossroadID($crossroadName)
    {
        $sth = $this->dbh->prepare('SELECT `id` FROM `crossroads` WHERE `name` = :crossroads_name ');
        $sth->bindValue('crossroads_name', $crossroadName, \PDO::PARAM_INT);
        $sth->execute();
        $array =$sth->fetch();
        return $array['id'];
    }

    /**
     * @param int $crossroadId
     * @return string
     * @internal param string $crossroadName
     */
    public function getCrossroadByID($crossroadId)
    {
        $sth = $this->dbh->prepare('SELECT `name` FROM `crossroads` WHERE `id` = :crossroads_id ');
        $sth->bindValue('crossroads_id', $crossroadId, \PDO::PARAM_INT);
        $sth->execute();
        $array =$sth->fetch();
        return $array['name'];
    }

    public function getCrossroadUrlByID($crossroadId)
    {
        $sth = $this->dbh->prepare('SELECT `URL` FROM `crossroads` WHERE `id` = :crossroads_id ');
        $sth->bindValue('crossroads_id', $crossroadId, \PDO::PARAM_INT);
        $sth->execute();
        $array =$sth->fetch();
        return $array['URL'];
    }

    /**
     * @param string $crossroadName
     */
    private function addIntoCrossroads($crossroadName){
        $this->dbh->insert('crossroads', array(
            'name' => $crossroadName,
            'security_key' => $this->generateRandomString()
        ));
    }

    /**
     * @param int $crossRoadAddedID
     */
    private function addIntoLights($crossRoadAddedID){
        for($i=1; $i<5; $i++)
        {
            $this->dbh->insert('lights', array(
                'crossroads_id' => $crossRoadAddedID,
                'external_light_id' => $i
            ));
        }
    }

    /**
     * @param int $crossRoadAddedID
     * @param int $greenTime
     * @param int $yellowTime
     */
    private function addIntoPhases($crossRoadAddedID,$greenTime,$yellowTime ){
        $this->dbh->insert('phases', array(
            'crossroads_id' => $crossRoadAddedID,
            'delay' => $greenTime,
            'phase_order'=>1,
            'actual'=>'yes'
        ));
        $this->dbh->insert('phases', array(
            'crossroads_id' => $crossRoadAddedID,
            'delay' => $yellowTime,
            'phase_order'=>2,
            'actual'=>'no'
        ));
        $this->dbh->insert('phases', array(
            'crossroads_id' => $crossRoadAddedID,
            'delay' => $greenTime,
            'phase_order'=>3,
            'actual'=>'no'
        ));
        $this->dbh->insert('phases', array(
            'crossroads_id' => $crossRoadAddedID,
            'delay' => $yellowTime,
            'phase_order'=>4,
            'actual'=>'no'
        ));

    }


}