<?php

namespace Sda\Trystar\Crossroad;


class Validator
{

    /**
     * @param $crossroadToValidate
     * @return bool
     */
    public function validate($crossroadToValidate)
    {
        foreach ($crossroadToValidate as $row) {
            if (
                array_key_exists('name', $row) &&
                array_key_exists('Green/red', $row) &&
                array_key_exists('Yellow', $row)
            ) {
                if ($row['name'] != '' && $row['Green/red'] % 5 == 0 && $row['Yellow'] % 5 == 0) {
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;

    }

    /**
     * @param $crossroadToValidate
     * @return bool
     */
    public function validateOneCrossroad($crossroadToValidate){

        if (
            array_key_exists('name', $crossroadToValidate) &&
            array_key_exists('Green/red', $crossroadToValidate) &&
            array_key_exists('Yellow', $crossroadToValidate)
        ) {
            if ($crossroadToValidate['name'] != '' && $crossroadToValidate['Green/red'] % 5 == 0 && $crossroadToValidate['Yellow'] % 5 == 0) {
                var_dump($crossroadToValidate);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $crossroadToValidate
     * @return bool
     */
    public function validateForUpdate($crossroadToValidate)
    {
        if (
            array_key_exists('id', $crossroadToValidate) &&
            array_key_exists('name', $crossroadToValidate) &&
            array_key_exists('Green/red', $crossroadToValidate) &&
            array_key_exists('Yellow', $crossroadToValidate)
        ) {
            if (
                $crossroadToValidate['id'] != '' &&
                $crossroadToValidate['name'] != '' &&
                $crossroadToValidate['Green/red'] % 5 == 0 &&
                $crossroadToValidate['Yellow'] % 5 == 0
            ) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}