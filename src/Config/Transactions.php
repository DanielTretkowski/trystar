<?php

namespace Sda\Trystar\Config;

use Doctrine\DBAL\Connection;


class Transactions
{
    /**
     * @var Connection
     */
    private $dbh;

    /**
     * Transactions constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    public function beginTransaction(){
        $this->dbh->beginTransaction();
    }
    public function commitTransaction(){
        $this->dbh->commit();
    }
    public function rollBackTransaction(){
        $this->dbh->rollBack();
    }
}



