<?php

namespace Sda\Trystar\Config;

class Config {

    const DB_CONNECTION_DATA=[
    'dbname' => 'trystar',
    'user' => 'root',
    'password' => '',
    'host' => 'localhost',
    'driver' => 'pdo_mysql'  
    ];

    const TEMPLATE_DIR =  __DIR__ . '/../View/template';

}
