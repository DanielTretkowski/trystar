<?php

namespace Sda\Trystar\Controller;

use Sda\Trystar\Phase\PhaseRepository;
use Sda\Trystar\Light\LightRepository;
use Sda\Trystar\Response\Response;
use Sda\Trystar\Crossroad\CrossroadRepository;

class WorkerController
{

    /**
     * @param PhaseRepository $repo
     */
    private $repo;
    /**
     * @param LightRepository $repoLight
     */
    private $repoLight;
    /**
     * @param Response $response
     */
    private $response;
    /**
     * @var CrossroadRepository
     */
    private $crossroadRepository;


    /**
     * WorkerController constructor.
     * @param PhaseRepository $repo
     * @param LightRepository $repoLight
     * @param Response $response
     * @param CrossroadRepository $crossroadRepository
     */
    public function __construct(
        PhaseRepository $repo,
        LightRepository $repoLight,
        Response $response,
        CrossroadRepository $crossroadRepository
    )
    {
        $this->repo = $repo;
        $this->repoLight = $repoLight;
        $this->response = $response;
        $this->crossroadRepository = $crossroadRepository;
    }

    public function run()
    {
//        TODO change to this code when more then one crossroad is set
        $crossroads = $this->crossroadRepository->getAllCrossroadsNoLimit();

        foreach ($crossroads as $crossroad) {
            $phase = $this->repo->getPhaseId($crossroad->getId());
            $actualPhase = $phase->getPhaseOrder();
            $newPhase = $this->repo->getNextPhase($crossroad->getId(), $actualPhase);
            if ($this->repo->compereTime($newPhase)) {
                $this->response->sendToCrossroad($this->repoLight->getLightsFromPhases($phase, $crossroad->getId()), $crossroad->getUrl());
                $this->repo->changePhaseId($crossroad->getId(), $actualPhase);
                $this->repo->addCurrentTime($phase->getPhaseOrder());
            }
        }

//        $crossroads = 1;
//
//        $phase = $this->repo->getPhaseId($crossroads);
//        $actualPhase = $phase->getPhaseOrder();
//        $newPhase = $this->repo->getNextPhase($crossroads, $actualPhase);
//        if ($this->repo->compereTime($newPhase)) {
//            $this->response->sendToCrossroad($this->repoLight->getLightsFromPhases($phase, $crossroads));
//            $this->repo->changePhaseId($crossroads, $actualPhase);
//            $this->repo->addCurrentTime($phase->getPhaseOrder());
//        }
//
//
    }
}
