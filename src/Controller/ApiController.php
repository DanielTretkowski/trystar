<?php

namespace Sda\Trystar\Controller;

use Sda\Trystar\Crossroad\CrossroadRepository;
use Sda\Trystar\Request\Request;
use Sda\Trystar\Response\Response;
use Sda\Trystar\Crossroad\CrossroadNotFoundException;
use Sda\Trystar\Crossroad\Validator;
use Sda\Trystar\Config\Transactions;
use Sda\Trystar\Validators\UserValidation;


class ApiController
{
    /**
     * @var Response
     */
    private $response;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var CrossroadRepository
     */
    private $crossroadRepository;
    /**
     * @var Validator
     */
    private $validator;
    /**
     * @var Transactions
     */
    private $transactions;
    /**
     * @var UserValidation
     */
    private $userValidation;

    /**
     * ApiController constructor.
     * @param Response $response
     * @param Request $request
     * @param CrossroadRepository $crossroadRepository
     * @param Validator $validator
     * @param Transactions $transactions
     * @param UserValidation $userValidation
     */
    public function __construct
    (
        Response $response,
        Request $request,
        CrossroadRepository $crossroadRepository,
        Validator $validator,
        Transactions $transactions,
        UserValidation $userValidation
    )
    {
        $this->response = $response;
        $this->request = $request;
        $this->crossroadRepository = $crossroadRepository;
        $this->validator = $validator;
        $this->transactions = $transactions;
        $this->userValidation = $userValidation;
    }

    public function run()
    {
        if ($this->request->getApiKeyFromHeader() != '' &&
            $this->userValidation->clientKeyValidation(
                $this->request->getApiKeyFromHeader()
            )
        ) {
            $action = $this->request->getParamFromGet('action');
            switch ($action) {
                case 'crossroads':
                    $method = $this->request->getHttpMethod();

                    switch ($method) {
                        case 'GET':
                            try {
                                $this->response->send($this->crossroadRepository->getAllCrossroadsNoLimit(), 0, Response::STATUS_OK);
                            } catch (CrossroadNotFoundException $e) {
                                $this->response->send('CrossRoads not found', 1, Response::STATUS_NOT_FOUND);
                            }
                            break;
                        case 'POST':
                            $crossroadData = $this->request->getJsonRequestBody();
                            if ($this->validator->validate($crossroadData)) {
                                $this->transactions->beginTransaction();
                                try {
                                    foreach ($crossroadData as $row) {
                                        try {
                                            $this->crossroadRepository->addNewCrossroad($row['name'], $row['Green/red'], $row['Yellow']);

                                        } catch (CrossroadNotFoundException $e) {
                                            $this->response->send('Crossroad ' . $row['name'] . ' was not added to data base', 1, Response::STATUS_NOT_FOUND);
                                        }
                                    }
                                    $this->transactions->commitTransaction();
                                } catch (CrossroadNotFoundException $e) {
                                    $this->transactions->rollBackTransaction();
                                    $this->response->send('Data was invalid', 1, Response::STATUS_NOT_FOUND);
                                }
                            } else {
                                $this->response->send('Data was invalid', 1, Response::STATUS_NOT_FOUND);
                            }
                            $this->response->send('All crossroads were added!', 0, Response::STATUS_OK);
                            break;
                    }
                    break;
                case 'crossroad':
                    $method = $this->request->getHttpMethod();
                    switch ($method) {
                        case 'GET':
                            try {
                                $this->response->send(
                                    $this->crossroadRepository
                                        ->getCrossroadByID(
                                            $this->request->getParamFromGet('id')), 0, Response::STATUS_OK);
                            } catch (CrossroadNotFoundException $e) {
                                $this->response->send('CrossRoads not found', 1, Response::STATUS_NOT_FOUND);
                            }
                            break;
                        case 'POST':
                            $crossroadData = $this->request->getJsonRequestBody();
                            if ($this->validator->validateOneCrossroad($crossroadData)) {
                                try {
                                    $this->crossroadRepository->addNewCrossroad($crossroadData['name'], $crossroadData['Green/red'], $crossroadData['Yellow']);
                                    $this->response->send('Crossroad ' . $crossroadData['name'] . ' was added to data base', 0, Response::STATUS_OK);
                                } catch (CrossroadNotFoundException $e) {
                                    $this->response->send('Crossroad ' . $crossroadData['name'] . ' was not added to data base', 1, Response::STATUS_NOT_FOUND);
                                }
                            } else {
                                $this->response->send('Invalid data', 1, Response::STATUS_NOT_FOUND);
                            }
                            break;
                        case 'PUT':
                            $crossroadData = $this->request->getJsonRequestBody();
                            if ($this->validator->validateForUpdate($crossroadData) && $this->crossroadRepository->getCrossroadByID((int)$crossroadData['id']) != '') {
                                try {
                                    $this->crossroadRepository->editCrossroad
                                    (
                                        $crossroadData['id'],
                                        $crossroadData['name'],
                                        $crossroadData['Green/red'],
                                        $crossroadData['Yellow']
                                    );
                                    $this->response->send('Crossroad was updated!', 0, Response::STATUS_OK);
                                } catch (CrossroadNotFoundException $e) {
                                    $this->response->send('Invalid data', 1, Response::STATUS_NOT_FOUND);
                                }
                            }
                            $this->response->send('Invalid data', 1, Response::STATUS_NOT_FOUND);
                            break;
                        case 'DELETE':
                            if ($this->crossroadRepository->getCrossroadByID((int)$this->request->getParamFromGet('id')) != '') {
                                try {
                                    $this->crossroadRepository->deleteCrossroad((int)$this->request->getParamFromGet('id'));
                                    $this->response->send('Crossroad with ' . $this->request->getParamFromGet('id') . ' id was deleted', 0, Response::STATUS_OK);
                                } catch (CrossroadNotFoundException $e) {
                                    $this->response->send('Invalid data', 1, Response::STATUS_NOT_FOUND);
                                }
                            } else {
                                $this->response->send('no crossroad ID value', 1, Response::STATUS_NOT_FOUND);
                            }
                            $this->response->send('There is no crossroad with ' . $this->request->getParamFromGet('id') . ' id', 1, Response::STATUS_NOT_FOUND);
                            break;
                    }
                    break;
                default:
                    echo 'Waiting...';
                    break;
            }
        } else {
            $this->response->send('Incorrect Auth Key', 1, Response::STATUS_NOT_FOUND);
        }
    }


}