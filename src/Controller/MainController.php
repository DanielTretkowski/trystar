<?php

namespace Sda\Trystar\Controller;

use Sda\Trystar\Request\Request;
use Sda\Trystar\Response\Response;
use Sda\Trystar\Validators\UserValidation;
use Sda\Trystar\View\Template;
use Sda\Trystar\Session\Session;
use Sda\Trystar\Crossroad\CrossroadRepository;
use Sda\Trystar\Phase\PhaseRepository;
use Sda\Trystar\Light\LightRepository;

class MainController
{

    /**
     * @var Request
     */
    private $request;
    /**
     * @var UserValidation;
     */
    private $userValidation;
    /**
     * @var Template
     */
    private $template;
    /**
     * @var int $currentPage
     */
    private $currentPage;
    /**
     * @var array $params
     */
    private $params = [
        "userName" => 'Enter user name',
        "password" => 'Enter your Password',
        "userInput" => '',
        "errorUser" => '',
        "errorPass" => ''
    ];
    /**
     * @var string $templateName
     */
    private $templateName = '';
    /**
     * @var string $userName
     */
    private $userName = '';
    /**
     * @var Session
     */
    private $session;
    /**
     * @var CrossroadRepository
     */
    private $crossroadRepository;
    /**
     * @var PhaseRepository
     */
    private $phaseRepository;
    /**
     * @var Response
     */
    private $response;
    /**
     * @var LightRepository
     */
    private $lightRepository;

    /**
     * MainController constructor.
     * @param Request $request
     * @param UserValidation $userValidation
     * @param Template $template
     * @param Session $session
     * @param CrossroadRepository $crossroadRepository
     * @param PhaseRepository $phaseRepository
     * @param Response $response
     * @param LightRepository $lightRepository
     */
    public function __construct
    (
        Request $request,
        UserValidation $userValidation,
        Template $template,
        Session $session,
        CrossroadRepository $crossroadRepository,
        PhaseRepository $phaseRepository,
        Response $response,
        LightRepository $lightRepository
    )
    {
        $this->request = $request;
        $this->userValidation = $userValidation;
        $this->template = $template;
        $this->session = $session;
        $this->crossroadRepository = $crossroadRepository;
        $this->phaseRepository = $phaseRepository;
        $this->response = $response;
        $this->lightRepository = $lightRepository;
    }

    public function run()
    {

        $action = $this->request->getParamFromGet('action', 'login');

        switch ($action) {
            case 'login':
                if ($this->session->getFromSession('userName') != '') {
                    $this->showMainView();
                } else {
                    $this->templateName = 'Login.html';
                }
                break;
            case 'Register':
                $this->userValidation->registerNewUser(
                    $this->request->getParamFromPost('name'),
                    $this->request->getParamFromPost('pass'),
                    $this->request->getParamFromPost('passRep')
                );
                $this->session->saveToSession('userName', $_POST['name']);
                $this->response->send('Welcome');
                break;
            case 'checkForUser':
                $this->userName = $this->request->
                getParamFromPost('name');
                $userValidationResponse = $this->userValidation->logInUser($this->userName, $this->request->getParamFromPost('pass'));
                switch ($userValidationResponse) {
                    case '1':
                        $this->session->saveToSession('userName', $this->userName);
                        $this->showMainView();
                        break;
                    case '2':
                        $this->templateName = 'Login.html';
                        $this->params =
                            [
                                'userName' => 'Wrong User',
                                'errorUser' => 'has-error',
                                'password' => 'Enter your Password'
                            ];
                        break;
                    case '3':
                        $this->templateName = 'Login.html';
                        $this->params =
                            [
                                'password' => 'Wrong Password',
                                'userInput' => $this->request->
                                getParamFromPost('name'),
                                'errorPass' => 'has-error'
                            ];
                        break;
                    default:
                        $this->templateName = 'Login.html';
                        break;
                }
                break;
            case 'crossroadsState':
                $this->showActualPhases();
                break;
            case 'changeLightPhase':
                $this->phaseRepository->changePhaseId(
                    $_POST['CrossroadID'],
                    $_POST['PhaseID']
                );
                $this->showActualPhases();
                $this->response->sendToCrossroad
                (
                    $this->params['lights'],
                    $this->crossroadRepository->getCrossroadUrlByID($_POST['CrossroadID'])
                );
                break;
            case 'logout':
                $this->session->sessionDestroy();
                $this->templateName = 'Login.html';
                break;
            case 'addNewCrossroad':
                $this->crossroadRepository->addNewCrossroad(
                    $this->request->getParamFromPost('crossroadName'),
                    $this->request->getParamFromPost('Green/red'),
                    $this->request->getParamFromPost('Yellow')
                );
                $this->showMainView();
                break;
            case 'removeCrossroad':
                $this->crossroadRepository->deleteCrossroad(
                    $this->request->getParamFromPost('CrossroadID')
                );
                break;
            default :
                $this->templateName = 'Login.html';
                break;
        }
        $this->template->renderTemplate($this->templateName, $this->params);
    }

    private function showActualPhases()
    {
        $this->templateName = 'Phases.html';
        $this->params = [
            'phases' => $this->phaseRepository->getPhaseId($_POST['CrossroadID']),
            'lights' => $this->lightRepository->getLightsFromPhases(
                $this->phaseRepository->getPhaseId($_POST['CrossroadID']),
                $_POST['CrossroadID']
            )
        ];
    }

    public function showMainView()
    {
        if ($this->request->getParamFromGet('page') == '' || $this->request->getParamFromGet('page') == null) {
            $this->currentPage = 1;
        } else {
            $this->currentPage = $this->request->getParamFromGet('page');
        }
        $this->templateName = 'MainView.html';
        $this->userName = $this->session->getFromSession('userName');
        $rowsNumber = ceil(($this->crossroadRepository->countRows()) / 5);
        $this->params = [
            'name' => $this->userName,
            'crossroads' => $this->crossroadRepository->getAllCrossroads($this->currentPage),
            'maxPages' => $rowsNumber,
            'thisPage' => $this->currentPage];
    }
}
