<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Sda\Trystar\Controller\ApiController;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Sda\Trystar\Response\Response;
use Sda\Trystar\Request\Request;
use Sda\Trystar\Crossroad\CrossroadRepository;
use Sda\Trystar\Config\Config;
use Sda\Trystar\Crossroad\Validator;
use Sda\Trystar\Config\Transactions;
use Sda\Trystar\Validators\UserValidation;


$config = new Configuration();
$dbh = DriverManager::getConnection(Config::DB_CONNECTION_DATA, $config);
$response = new Response();
$request = new Request();
$crossroadRepository = new CrossroadRepository($dbh);
$validator = new Validator();
$transactions = new Transactions($dbh);
$userValidation = new UserValidation($dbh,$response);

$app = new ApiController
(
    $response,
    $request,
    $crossroadRepository,
    $validator,
    $transactions,
    $userValidation
);
$app->run();