<?php


require_once __DIR__ . '/../vendor/autoload.php';


use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Sda\Trystar\Controller\MainController;
use Sda\Trystar\Request\Request;
use Sda\Trystar\Validators\UserValidation;
use Sda\Trystar\Config\Config;
use Sda\Trystar\View\Template;
use Sda\Trystar\Session\Session;
use Sda\Trystar\Crossroad\CrossroadRepository;
use Sda\Trystar\Phase\PhaseRepository;
use Sda\Trystar\Response\Response;
use Sda\Trystar\Light\LightRepository;


$loader = new Twig_Loader_Filesystem(Config::TEMPLATE_DIR);
$twig = new Twig_Environment(
    $loader,
    [
        'cache' => false,
        'debug' => true
    ]
);
$twig->addExtension(new Twig_Extension_Debug());
$template = new Template($twig);
$config = new Configuration();
$dbh = DriverManager::getConnection(Config::DB_CONNECTION_DATA, $config);
$request = new Request();
$response = new Response();
$userValidation = new UserValidation($dbh,$response);
$session = new Session();
$crossroadRepository = new CrossroadRepository($dbh);
$phaseRepository = new PhaseRepository($dbh);
$lightRepository = new LightRepository($dbh);



$app = new MainController
(
    $request,
    $userValidation,
    $template,
    $session,
    $crossroadRepository,
    $phaseRepository,
    $response,
    $lightRepository
);
$app->run();

