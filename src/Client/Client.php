<?php

namespace Sda\Trystar\Client;


class Client
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $userName;
    /**
     * @var string
     */
    private $userPass;
    /**
     * @var string
     */
    private $LoginDate;
    /**
     * @var string
     */
    private $apiKey;

    /**
     * @param int $id
     * @param string $userName
     * @param string $userPass
     * @param string $LoginDate
     * @param String $apiKey
     */
    public function __construct($id, $userName, $userPass, $LoginDate, $apiKey){

        $this->id = $id;
        $this->userName = $userName;
        $this->userPass = $userPass;
        $this->LoginDate = $LoginDate;
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @return string
     */
    public function getUserPass()
    {
        return $this->userPass;
    }

    /**
     * @param string $userPass
     */
    public function setUserPass($userPass)
    {
        $this->userPass = $userPass;
    }

    /**
     * @return string
     */
    public function getLoginDate()
    {
        return $this->LoginDate;
    }

    /**
     * @param string $LoginDate
     */
    public function setLoginDate($LoginDate)
    {
        $this->LoginDate = $LoginDate;
    }
}