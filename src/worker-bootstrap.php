<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Sda\Trystar\Controller\WorkerController;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Sda\Trystar\Config\Config;
use Sda\Trystar\Phase\PhaseRepository;
use Sda\Trystar\Light\LightRepository;
use Sda\Trystar\Response\Response;
use Sda\Trystar\Crossroad\CrossroadRepository;

$config = new Configuration();
$dbh = DriverManager::getConnection(Config::DB_CONNECTION_DATA, $config);
$repo =new PhaseRepository($dbh);
$repoLight =new LightRepository($dbh);
$response = new Response();
$crossroadRepository = new CrossroadRepository($dbh);
$app = new WorkerController($repo, $repoLight,$response, $crossroadRepository);
$app->run();
