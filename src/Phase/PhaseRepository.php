<?php

namespace Sda\Trystar\Phase;

use Doctrine\DBAL\Connection;

class PhaseRepository {
    /**
     * @var $dbh
     */
    private $dbh;

    /**
     * PhaseRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh) {
        $this->dbh=$dbh;
    }

    /**
     * @param int $crossroads_id
     * @return Phase
     */
    public function getPhaseId($crossroads_id){
        
        $sth = $this->dbh->prepare('SELECT * FROM `phases` WHERE `crossroads_id` = :crossroads_id AND `actual` = \'yes\'' );
        $sth->bindValue('crossroads_id', $crossroads_id, \PDO::PARAM_INT);
        $sth->execute();

       $loginData = $sth->fetch();
       $phase = new Phase($loginData['id'], $loginData['delay'], $loginData['phase_order'], $loginData['actual']);
       return $phase;
    }

    /**
     * @param int $crossroads_id
     * @param int $actualPhase
     * @return Phase
     */
    public function getNextPhase($crossroads_id, $actualPhase){

        if($actualPhase<$this->countPhases($crossroads_id)){
            $actualPhase++;
        }else{
            $actualPhase=1;
        }
        $sth = $this->dbh->prepare('SELECT * FROM `phases` WHERE `crossroads_id` = :crossroads_id AND `phase_order` = :nextPhase' );
        $sth->bindValue('crossroads_id', $crossroads_id, \PDO::PARAM_INT);
        $sth->bindValue('nextPhase', $actualPhase, \PDO::PARAM_INT);
        $sth->execute();
        
       $loginData = $sth->fetch();
       $phase = new Phase($loginData['id'], $loginData['delay'], $loginData['phase_order'], $loginData['actual']);
       return $phase;
    }
     /**
      * @param int $crossroads_id
      * @param int $actualPhase
     */
    public function changePhaseId($crossroads_id, $actualPhase){
        
          if($actualPhase<$this->countPhases($crossroads_id)){
            $this->dbh->executeUpdate('UPDATE phases SET actual = ? WHERE crossroads_id = ? AND phase_order = ?' ,array('no', $crossroads_id, $actualPhase));
            $actualPhase++;
            $this->dbh->executeUpdate('UPDATE phases SET actual = ? WHERE crossroads_id = ? AND phase_order = ?' ,array('yes', $crossroads_id, $actualPhase));
        }else{
            $this->dbh->executeUpdate('UPDATE phases SET actual = ? WHERE crossroads_id = ? AND phase_order = ?' ,array('no', $crossroads_id, $actualPhase));
            $actualPhase=1;
            $this->dbh->executeUpdate('UPDATE phases SET actual = ? WHERE crossroads_id = ? AND phase_order = ?' ,array('yes', $crossroads_id, $actualPhase));
        }
    }

    /**
     * @param int $crossroads_id
     * @return int $phasesCount
     */
        public function countPhases($crossroads_id){
         $sth = $this->dbh->prepare('SELECT COUNT(*) FROM `phases` WHERE `crossroads_id` = :crossroads_id' );
         $sth->bindValue('crossroads_id', $crossroads_id, \PDO::PARAM_INT);
         $sth->execute();
         return $phasesCount = $sth->fetchColumn();
    }
    
     /**
      * @param Phase $phase
      * @return bool
     */
    public function compereTime(Phase $phase){
        
         $sth = $this->dbh->prepare('SELECT time FROM `lightsdata` ORDER BY `time` DESC LIMIT 1');
         $sth->execute();
         $timeStamp = $sth->fetch();
         $timeFromData= strtotime($timeStamp['time']);
         $timeNow = time();
                 
         if(($timeNow-$timeFromData) >(int)$phase->getDelay() ){
             return true;
         }
         return false;
    }
    
    /**
      * @param Phase $phase
     */
    public function addCurrentTime($phase){
        $timeNow = date('Y-m-d H:i:s', time());
        $array = [
            'phase_id'=>$phase,
            'time'=>$timeNow
        ];
        $this->dbh->insert('lightsdata',$array);
    }
    
    
}
