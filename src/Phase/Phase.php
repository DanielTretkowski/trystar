<?php

namespace Sda\Trystar\Phase;

class Phase {
    /**
     * @var $id
     */
    private $id;
    /**
     * @var $delay
     */
    private $delay;
    /**
     * @var $phaseOrder
     */
    private $phaseOrder;
    /**
     * @var $actual
     */
    private $actual;


    /**
     * Phase constructor.
     * @param int $id
     * @param int $delay
     * @param int $phaseOrder
     * @param string $actual
     */
    public function __construct($id, $delay, $phaseOrder, $actual) {
        $this->id = $id;
        $this->delay = $delay;
        $this->phaseOrder = $phaseOrder;
        $this->actual=$actual;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getDelay()
    {
        return $this->delay;
    }

    /**
     * @return int
     */
    public function getPhaseOrder()
    {
        return $this->phaseOrder;
    }

    /**
     * @return string
     */
    public function getActual()
    {
        return $this->actual;
    }
}
