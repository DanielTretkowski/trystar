<?php

namespace Sda\Trystar\Light;

class Light implements \JsonSerializable {
    /**
     * @var $id
     */
    private $id;
    /**
     * @var $crossroadsId
     */
    private $crossroadsId;
    /**
     * @var $externalLightId
     */
    private $externalLightId;
    /**
     * @var $state
     */
    private $state;


    /**
     * Light constructor.
     * @param int $id
     * @param int $crossroadsId
     * @param int $externalLightId
     * @param string $state
     */
    public function __construct($id, $crossroadsId, $externalLightId, $state) {
        $this->id = $id;
        $this->crossroadsId = $crossroadsId;
        $this->externalLightId = $externalLightId;
        $this->state=$state;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCrossroadsId()
    {
        return $this->crossroadsId;
    }

    /**
     * @return int
     */
    public function getExternalLightId()
    {
        return $this->externalLightId;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' =>  $this->externalLightId,
            'state'=>  $this->state
        ];
    }
}
