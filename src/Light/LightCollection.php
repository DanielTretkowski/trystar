<?php

namespace Sda\Trystar\Light;

use Sda\Trystar\TypedCollection;

class LightCollection extends TypedCollection
{
    /**
     * LightCollection constructor.
     */
    public function __construct()
    {
        $this->setItemType(Light::class);
    }
}