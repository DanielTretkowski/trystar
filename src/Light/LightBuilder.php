<?php


namespace Sda\Trystar\Light;



class LightBuilder {
    /**
     * @var $id
     */
    private $id;
    /**
     * @var $crossroadsId
     */
    private $crossroadsId;
    /**
     * @var $externalLightId
     */
    private $externalLightId;
    /**
     * @var $state
     */
    private $state;


    /**
     * @return Light
     */
    public function build()
    {
        return new Light(
            $this->id,
            $this->crossroadsId,
            $this->externalLightId,  
            $this->state
        );
    }

    /**
     * @param int $id
     * @return LightBuilder
     */
    public function withId($id)
    {
        $this->id = $id;
        return $this;
    }
    /**
     * @param int $crossroadsId
     * @return LightBuilder
     */
    public function withCrossRoadsId($crossroadsId){
        $this->crossroadsId = $crossroadsId;
        return $this;
    }
    /**
     * @param int $externalLightId
     * @return LightBuilder
     */
     public function withExternalLightId($externalLightId){
        $this->externalLightId = $externalLightId;
        return $this;
    }
    /**
     * @param string $state
     * @return LightBuilder
     */
     public function withState($state)
    {
        $this->state = $state;
        return $this;
    }
}
