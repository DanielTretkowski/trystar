<?php



namespace Sda\Trystar\Light;
use Sda\Trystar\Phase\Phase;
use Doctrine\DBAL\Connection;

class LightRepository {
    /**
     * @var $dbh
     */
    private $dbh;

    /**
     * LightRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh) {
        $this->dbh=$dbh;
    }
     /**
     * @param Phase $phase
     * @param int $crossroads
     * @return LightCollection
      */
    public function getLightsFromPhases($phase, $crossroads){
                

         $sth = $this->dbh->prepare('SELECT * FROM `lights` AS l JOIN `lights_states` AS'
                 . ' ls ON ls.`lights_id` = l.`external_light_id` WHERE l.`crossroads_id` =:crossroads_id AND ls.`phases_id` =:phases_id');
         $sth->bindValue('crossroads_id', $crossroads, \PDO::PARAM_INT);
         $sth->bindValue('phases_id', $phase->getPhaseOrder(), \PDO::PARAM_INT);
         $sth->execute();
         $loginData = $sth->fetchAll();
         $lights= new LightCollection();
         
         foreach($loginData as $row){

             $builder = new LightBuilder();
             $light = $builder
            ->withId((int)$row['id'])
            ->withCrossRoadsId($row['crossroads_id'])
            ->withExternalLightId($row['external_light_id'])  
            ->withState($row['state'])
            ->build();

             $lights->add($light);
         }
         return $lights;
    }
}
