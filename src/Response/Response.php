<?php

namespace Sda\Trystar\Response;


class Response {
    
    const STATUS_OK = 200;
    const STATUS_BAD_REQUEST = 400;
    const STATUS_PERMISION_DENIED = 403;
    const STATUS_NOT_FOUND = 404;
    const CROSSROAD_URL = 'http://crossroad.local/api/lights';

    /**
     * @param string $dataToSend
     * @param int $errorCode
     * @param int $statusCode
     */
    public function send($dataToSend, $errorCode = 0, $statusCode = self::STATUS_OK){
        
        http_response_code($statusCode);
        $data = [
            'errors' => [],
            'data' => $dataToSend,
            'errorCode' => $errorCode
        ];
        echo json_encode($data);
        exit();
    }

    /**
     * @param $lightCollection
     * @param string $url
     */
    public function sendToCrossroad($lightCollection, $url = self::CROSSROAD_URL)
    {
        $jsonData = json_encode($lightCollection);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 1,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $jsonData,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            die('Problem z wysylka');
        }
    }

}
