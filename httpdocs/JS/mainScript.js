var main = {
    crossroadId: '',
    startup: function () {
        this.mainViewWithCrossroads();

        $('#addNewCrossroad').on('click', function (e) {
            $('#MainFormFirstPage').hide('slow');
            $('#MainFormFirstPage2').hide('slow');
            $('#MainFormFirstPage3').show('slow');

            e.preventDefault();
        });

        $('#RegisterFormBtn').on('click', function (e) {
            $('#MainFormFirstPage').hide('slow');
            $('#RegisterPage').show('slow');

            e.preventDefault();
        });
        $('#backButton').on('click', function (e) {
            $('#MainFormFirstPage').show('slow');
            $('#RegisterPage').hide('slow');

            e.preventDefault();
        });

        $('#RegisterBtn').click(function (e) {

            var data = {};
            $(this).parents('form').children().find('input').not('[type="submit"]').each(function () {
                data[$(this).attr('name')] = $(this).val();
            });

            $.ajax({
                type: "POST",
                url: "?action=Register",
                data: data,
                dataType: 'json',
                success: function (data) {
                    switch (data.errorCode) {
                        case 0:
                            window.location = '?action=login';
                            break;
                        case 1:
                            $('#nameUserRegistration').parent().addClass('has-error');
                            $('#nameUserRegistration').attr("placeholder", data.data);
                            break;
                        case 2:
                            $('#nameUserRegistration').parent().removeClass('has-error');
                            $('.passwordRejest').parent().addClass('has-error');
                            $('.passwordRejest').attr("placeholder", data.data);
                            break;
                        default:
                            break;
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
            e.preventDefault();
        });
    },

    mainViewWithCrossroads: function (e) {
        $('.removeBtn').on('click', function () {
            main.crossroadId = $(this).parent().attr('id');

            $.ajax({
                type: "POST",
                url: "?action=removeCrossroad",
                data: {CrossroadID: main.crossroadId},

                success: function (data) {

                    location.reload();
                }
            });
            return false;
        });
        $('#crossroadsInfo li').on('click', function () {
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            main.crossroadId = $(this).attr('id');

            $.ajax({
                type: "POST",
                url: "?action=crossroadsState",
                data: {CrossroadID: main.crossroadId},

                success: function (data) {
                    $('#MainFormFirstPage').removeClass("col-md-offset-4");
                    $('#MainFormFirstPage').addClass("col-md-offset-2");
                    $('#MainFormFirstPage2').html(data).show('slow');
                }
            });
            e.preventDefault();
        })
    },

    PhasesAndLights: function (e) {
        $('#ChangeLightPhase').on('click', function (e) {

            var phase = $('.phaseGroup').text();

            $.ajax({
                type: "POST",
                url: "?action=changeLightPhase",
                data: {
                    PhaseID: phase,
                    CrossroadID: main.crossroadId
                },

                success: function (data) {
                    if ($('#iframe').is(':visible')) {
                        $('#MainFormFirstPage2').html(data)
                        $('#phasesToChange').hide();
                        $('#iframe').show();
                    } else {
                        $('#MainFormFirstPage2').html(data);
                    }
                }
            });
            e.preventDefault();
        });
        $('#showActualCrossroad').on('click', function (e) {
            if ($('#iframe').is(':visible')) {
                $('#phasesToChange').show('slow');
                $('#iframe').hide('slow');
            } else {
                $('#phasesToChange').hide('slow');
                $('#iframe').show('slow');
            }
            e.preventDefault();
        });
    }
};

var wizard = {

    startWizard: function () {

        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid) {
                nextStepWizard.removeAttr('disabled').trigger('click');
            }
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
    }
};

$(document).ready(function () {
    main.startup();
    wizard.startWizard();
});